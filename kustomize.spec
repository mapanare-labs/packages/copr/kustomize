%define debug_package %{nil}

Name:           kustomize
Version:        5.5.0
Release:        1%{?dist}
Summary:        Customization of kubernetes YAML configurations

License:        ASL 2.0
URL:            https://kustomize.io/
Source0:        https://github.com/kubernetes-sigs/%{name}/releases/download/%{name}%2Fv%{version}/%{name}_v%{version}_linux_amd64.tar.gz

%description
Kustomize introduces a template-free way to customize 
application configuration that simplifies the use of 
off-the-shelf applications. Now, built into kubectl as apply -k.

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_bindir}
install -m 755 %{name} %{buildroot}/%{_bindir}/%{name}

%files
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.3.0

* Tue Nov 14 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.2.1

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.1.1

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.1.0

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.0.3

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.0.2

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.0.1

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 5.0.0

* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM